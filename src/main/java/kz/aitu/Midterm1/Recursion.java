package kz.aitu.Midterm1;

//Test: 1.B, 2.A, 3.B, 4.D, 5.E

public class Recursion {
    public static void main (String[] args){

        System.out.println(Reduce("yyyzzza"));
        System.out.println(Reduce("abbbcdd"));
        System.out.println(Reduce("Hello"));
    }

    public static String Reduce (String word) {
        for(int i=1; i<(word.length()); i++){
            if(word.charAt(i - 1) == word.charAt(i))
                return Reduce(word.substring(0, i - 1) + word.substring(i, word.length()));
        }
            return word;

    }
}
