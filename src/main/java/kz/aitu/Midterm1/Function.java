package kz.aitu.Midterm1;

public class Function {

    public static int Purchase (int arr[], int n){
        int buy = arr[0];
        for(int i=0; i<n; i++){
            buy = Math.min(buy, arr[i]);
        }
        return buy;
    }
    public static int Sale (int arr[], int n){
        int sale = arr[0];
        for(int i=0; i<n; i++){
            sale = Math.max(sale, arr[i]);
        }
        return sale;
    }
    public static void main (String[] args){

        int arr[] = {10, 7, 5, 8, 11, 9};
        int n = arr.length;
        System.out.println(Purchase(arr, n));
        System.out.println(Sale(arr, n));
    }

}
