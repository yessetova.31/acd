package kz.aitu.Midterm1.Linkedlist;

public class LinkedList {
    private Node head;

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getHead() {
        return head;
    }

    public void insertAt(int data) {
        Node current = new Node();
        current = current.getNext();
        head = current;
    }


    public void removeAt(int position) {
        if (head == null)
            return;
        Node temp = head;
        if (position == 0) {
            head = temp.getNext();
            return;
        }
        for (int i=0; temp!=null && i<position-1; i++)
            temp = temp.getNext();

        if (temp == null || temp.getNext() == null)
            return;
        Node next = temp.getNext().getNext();
        next = temp.getNext();
    }


    public void show(){
        Node current = head;
        while (current.getNext()!= null) {
            System.out.println(current.getData());
            current= current.getNext();
        }
        System.out.println(current.getData());
    }
}


