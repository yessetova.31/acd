package kz.aitu.week6BST;

public class Tree {
    private static Node root;

    public Tree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);
        if (root == null)
            root = node;
        else {
            Node current = root;
            while (current != null) {
                if (key < current.getKey()) {
                    if (current.getLeft() == null) {
                        current.setLeft(node);
                        return;
                    }
                    current = current.getLeft();
                } else if (key > current.getKey()) {
                    if (current.getRight() == null) {
                        current.setRight(node);
                        return;
                    }
                    current = current.getRight();
                }
            }
        }
    }



    public boolean delete (Integer key) {
        // 0.empty
        if (root == null);
        //1. root nu;;
        if (root.getKey() == key){
            if(root.getLeft() == null && root.getRight()==null)
                root = null;
        }
        Node current = root;
        // 1.Delete if leaf
        while (current != null) {
            if (key < current.getKey()) {
                if (current.getLeft() != null && current.getLeft().getKey() .equals(key))
                    current.setLeft(null);
                current = current.getLeft();
            } else {
                if (current.getRight() != null && current.getRight().getKey().equals(key))
                    current.setRight(null);
                current = current.getRight();
            }
        }

        // 4. Delete root if one child
        if (current.getLeft() == null) {
            root = current.getRight();
        } else {
            root = current.getLeft();
        }

        //5. Delete root if two child
        while (current!= null)
        if (current.getLeft() != null && current.getRight() != null){
            current = current.getLeft();
            while (current.getRight() != null) {
                root = current.getRight();
                current.setRight(null);
            }
        }


        return false;
    }



    public String find (Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {

    }

    public void printAll() {

    }

}
