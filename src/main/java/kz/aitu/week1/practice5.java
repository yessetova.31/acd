package kz.aitu.week1;

public class practice5 {
    static int x(int n)
    {
        if (n <= 1)
            return n;
        return x(n-1) + x(n-2);
    }

    public static void main (String args[])
    {
        int n = 5;
        System.out.println(x(n));
    }
}
