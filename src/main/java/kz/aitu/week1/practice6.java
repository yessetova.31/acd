package kz.aitu.week1;

public class practice6 {
    static int b(int a, int n)
    {
        if (n == 0)
            return 1;
        else if (n % 2 == 0)
            return b(a, n / 2) * b(a, n / 2);
        else
            return a * b(a, n / 2) * b(a, n / 2);
    }

    /* Program to test function power */
    public static void main(String[] args)
    {
        int a = 2;
        int n = 3;

        System.out.printf("%d", b(a, n));
    }
}

