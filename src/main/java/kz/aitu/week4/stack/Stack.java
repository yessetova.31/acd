package kz.aitu.week4.stack;

public class Stack {
    private Node head;

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getHead() {
        return head;
    }

    public void push(String data){
        Node node = new Node(data);
        if (head == null){
            head = node;
        } else {
            node.setNext(head);
            head = node;
        }
    }

    public void pop(){
        Node currentB = head;
        head = currentB.getNext();
    }

    public void empty(){
        if (head==null){
            System.out.println("Is empty");
        } else {
            System.out.println("Isn't empty");
        }
    }

    public int size(Stack stack){
        Node top = stack.getHead();
        int cursize = 0;
        while (top!=null){
            top =top.getNext();
            cursize++;
        }
        return cursize;
    }

    public void Top(){
        System.out.println(head.getData());
    }
}

