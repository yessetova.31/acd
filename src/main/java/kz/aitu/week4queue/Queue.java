package kz.aitu.week4queue;

public class Queue {
    private Node head;
    private Node tail;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getTail() {
        return tail;
    }

    public void add (String data){
        Node node = new Node(data);
        if (head==null){
            head = node;
            tail = node;
        } else {
            tail.setNext(node);
            tail = node;
        }
    }

    public void poll () {
        System.out.println(head.getData());
        head = head.getNext();
    }

    public void peek () {
        System.out.println(head.getData());
    }

    public int size(){
        int cursize = 0;
        while (head!=null){
            head = head.getNext();
            cursize++;
        }
        return cursize;
    }

}
