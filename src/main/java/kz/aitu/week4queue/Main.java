package kz.aitu.week4queue;

public class Main {
    public static void main (String[] args) {
        Queue queue = new Queue();
        queue.add("apple");
        queue.add("book");
        queue.add("pen");
        queue.add("stop");
        queue.peek();
        queue.poll();

        System.out.println("Size: " + queue.size());
    }
}
