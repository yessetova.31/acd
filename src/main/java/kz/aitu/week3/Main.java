package kz.aitu.week3;

public class Main {
    public static void main (String[] args) {
        Node n1 = new Node();
        Node n2 = new Node();
        Node n3 = new Node();
        Node n4 = new Node();
        Node n5 = new Node();
        Node n6 = new Node();



        n1.setData("Student");
        n2.setData("Orange");
        n3.setData("Car");
        n4.setData("Doll");
        n5.setData("Toy");
        n6.setData("Book");


        n1.setNext(n2);
        n2.setNext(n3);
        n3.setNext(n4);
        n4.setNext(n5);
        n5.setNext(n6);
        n6.setNext(null);



        Node n7 = new Node();
        n7.setData("iPhone");
        n2.setNext(n7);
        n7.setNext(n3);

        Node n8= new Node();
        n8.setData("eight");
        n8.setNext(n1);


        Node n9=new Node();
        n9.setData("Nine");
        Node current1=n8;
        while (current1.getNext()!=null){
            current1 = current1.getNext();
        }

        current1.setNext(n9);
        n9.setNext(null);

        Node current = n8;
        while (current.getNext() != null) {
            System.out.println(current.getData());
            current = current.getNext();
        }

        int current2Size = 0;
        Node current2 = n8;
        while (current2 != null) {
            current2Size = current2Size + 1;
            current2 = current2.getNext();
        }
        System.out.println("Size:" + current2Size);
    }

}
