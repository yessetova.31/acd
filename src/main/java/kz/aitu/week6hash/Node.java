package kz.aitu.week6hash;

public class Node {
    private String key;
    private String value;
    private Node next;

    public Node(String key, String value) {
        this.key = key;
        this.value = value;
        this.next = null;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int hashCode() {

        return key.hashCode();
    }

    public void setNext(Node next) {

        this.next = next;
    }

    public Node getNext() {

        return next;
    }

    public String getValue() {

        return value;
    }

    public Node setValue(String value) {
        this.value = value;
        return null;
    }
}

