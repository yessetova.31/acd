package kz.aitu.week6hash;

public class Hashtable {
    private Node[] table;
    private int size;

    public Hashtable(int size) {
        table = new Node[size];
        this.size = size;
    }

    public void set(String key, String value) {
        int index = key.hashCode() % size;
        Node current = table[index];
        if (current == null) {
            table[index] = new Node(key, value);
        } while (current != null){
            if (current.getKey() == key){
                current.setValue(value);
            } else if (current.getNext() == null) {
                current.setNext(new Node(key, value));
            }
            current = current.getNext();
        }
    }


    public String get(String key) {
        int index = key.hashCode() % size;
        Node current = table[index];
        if (table == null) {
            return table[index].getValue();
        } else {
            while (current != null){
                if (current.getKey() == key){
                    return current.getValue();
                }
                current = current.getNext();
            }
            return null;
        }
    }


    public void remove(String key) {
        int index = key.hashCode() % size;
        Node current = table[index];
        if (current == null){
            return;
        }
            if (current.getKey() == key) {
                current = current.getNext();
                return;
            } while(current.getNext()!=null) {
                if (current.getNext().getKey() == key) {
                    current.setNext(current.getNext().getNext());
                    return;
                }
            current = current.getNext();

        }
    }

    public int getSize() {
        int sizecurrent=0;
        for (int i=0; i<table.length; i++)
        if (table[i] != null)
            sizecurrent++;
        return sizecurrent;
    }

    public void printAll() {
        for (int i = 0; i < table.length; i++) {
            Node j = table[i];

            System.out.print(i + ": ");
            while(j != null) {
                System.out.print(j.getKey() + "-" + j.getValue() + "\t\t>");
                j = j.getNext();
            }
            System.out.println();
        }
    }

}
