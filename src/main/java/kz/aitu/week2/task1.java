package kz.aitu.week2;

public class task1 {
        static int x(int n)
        {
            if (n <= 1)
                return n;
            return x(n-1) + x(n-2);
        }

        public static void main (String args[])
        {
            int n = 10;
            System.out.println(x(n));
        }
    }
