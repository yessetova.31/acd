package kz.aitu.week2;

public class task2 {
    static void a(int arr[],
                             int start, int end)
    {
        int temp;

        while (start < end)
        {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }
    static void b(int arr[],
                           int size)
    {
        for (int i = 0; i < size; i++)
            System.out.print(arr[i] + " ");

        System.out.println();
    }

    public static void main(String args[]) {

        int arr[] = {1, 2, 3, 4, 5, 6};
        b(arr, 6);
        a(arr, 0, 5);
        b(arr, 6);

    }
}
