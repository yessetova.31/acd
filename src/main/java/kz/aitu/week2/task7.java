package kz.aitu.week2;

public class task7 {
    static void x(char[] set, int k)
    {
        int n = set.length;
        x(set, "", n, k);
    }
    static void x(char[] set,
                                   String y,
                                   int n, int k)
    {
        if (k == 0)
        {
            System.out.println(y);
            return;
        }
        for (int i = 0; i < n; ++i)
        {
            String newy = y + set[i];
            x(set, newy,
                    n, k - 1);
        }
    }
    public static void main(String[] args)
    {
        char[] set1 = {'a', 'b'};
        int k = 3;
        x(set1, k);
    }
}
