package kz.aitu.week7.bubble;

import static kz.aitu.week7.bubble.BonusPoints.*;

public class Main {

    /* public static class CS1903 {

         BonusPoints bubblePoints = new BonusPoints();
         BonusPoints selectionPoints = new BonusPoints();
         BonusPoints insertionPoints = new BonusPoints();
         BonusPoints mergePoints = new BonusPoints();
         BonusPoints quickPoints = new BonusPoints();

         public static void main(String[] args) {
             CS1903 cs1903 = new CS1903();
             cs1903.sort_bubble();
             System.out.println(cs1903.bubblePoints.getPoints("Student1")); //returns 93
         }

         public void sort_bubble() {
             bubblePoints.addRoot("DIAS");
             bubblePoints.addRoot("SANZHAR");
             bubblePoints.addRoot("DAMIR");
             bubblePoints.addRoot("BAHA");
         }

         public void sort_selection() {
             bubblePoints.addRoot("SANZHAR");
             bubblePoints.addRoot("DAMIR");
         }

         public void sort_insertion() {
         }

         public void sort_merge() {
             bubblePoints.addRoot("DIAS");
             bubblePoints.addRoot("SANZHAR");
             bubblePoints.addRoot("ABILAYKHAN");
             bubblePoints.addRoot("BAHA");
         }

         public void sort_quick() {
             bubblePoints.addRoot("SANZHAR");
             bubblePoints.addRoot("DAMIR");
         }
     }
 */
    public static void main(String[] args) {

        int[] arr = {5, 8, 3, 7, 9, 6,8 ,9 , 10, 123, 14};
        insertionPoints(arr);

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}
