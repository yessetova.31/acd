package kz.aitu.week7.bubble;

public class BonusPoints {

    public static void bubblePoints(int[] arr){
        for(int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr.length - i - 1; j++){
                if (arr[j] > arr[j+1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
    public static void selectionPoints(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int minVal = arr[i];
            int minInd = i;
            for(int b = i + 1; b < arr.length; b++){
                if(arr[b] < minVal) {
                    minVal = arr[b];
                    minInd = b;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minInd];
            arr[minInd] = temp;

        }

    }
    public static void insertionPoints(int[] arr){
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++){
                if (arr[i] <= arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    public static void quickPoints(int[] a) {

        quick(a,0,a.length - 1);
    }

    private static void quick(int[] arr, int first, int last){
        if (first < last) {
            int j = first - 1;
            for (int i = first; i < last; i++) {
                if (arr[i] < arr[last]) {
                    j++;
                    int temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
            j++;
            int temp = arr[j];
            arr[j] = arr[last];
            arr[last] = temp;
            quick(arr, first, j - 1);
            quick(arr, j + 1, last);
        } else return;
    }

    }

