package kz.aitu.Endterm;

public class BST {

    private static Node root;

    public BST() {
        this.root = null;
    }

    public void insert(Integer key, Integer value) {
        Node node = new Node (key, value);
        if (root == null)
            root = node;
        else {
            Node current = root;
            while (current != null) {
                if (key < current.getKey()) {
                    if (current.getLeft() == null) {
                        current.setLeft(node);
                        return;
                    }
                    current = current.getLeft();
                } else if (key > current.getKey()) {
                    if (current.getRight() == null) {
                        current.setRight(node);
                        return;
                    }
                    current = current.getRight();
                }
            }
        }
    }
    //Second
     Node count(Node root){
        if(root.equals(null)) {
            System.out.println(0);
        } else if (root.getValue()%2 == 1) {
            System.out.println(root.getValue());
            System.out.println(count(root.getLeft()));
            System.out.println(count(root.getRight()));

        }  return root;
    }

    //First
   Node ica(Node root, int value1, int value2){

        if(root.equals(null))
            return null;
        if (value1>value2){
            int temp = 2;
            value2 = value1;
            value1 = temp;
        }
        while (root.getValue() < value1 || root.getValue() > value2){
            if(root.getValue() < value1) {
                root = root.getRight();
            } else if (root.getValue() > value2){
                root = root.getLeft();
            }
        }
        return root;
    }

    //Third I tried to find minimum but it's just find value(((
    public Node findminimum (Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey())
                current = current.getRight();
            else if(key < current.getKey())
                current = current.getLeft();
            else
                System.out.println(current.getValue());
        }
        return null;
    }


}

package kz.aitu.Endterm;

public class Node {
    private Integer key;
    private Integer value;
    private Node left;
    private Node right;

    public Node(Integer key, Integer value) {
        this.key = key;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getLeft() {
        return left;
    }
}
